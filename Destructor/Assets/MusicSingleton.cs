﻿using UnityEngine;
using System.Collections;

public class MusicSingleton : MonoBehaviour
{
    // Private variables
    // Static variables
    private static MusicSingleton instance;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(instance);
    }
}
