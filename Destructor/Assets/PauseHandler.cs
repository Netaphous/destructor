﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Netaphous.Destructor
{
    public class PauseHandler : MonoBehaviour
    {
        // Private variables
        // Accessible in the editor
        [SerializeField]
        private GameObject confirmationPanel;
        [SerializeField]
        private GameObject content;

        private List<AudioSource> soundSources;

        void OnEnable()
        {
            AudioSource[] tempSources = FindObjectsOfType<AudioSource>();

            soundSources = new List<AudioSource>();

            int counter = 0;

            foreach(AudioSource source in tempSources)
            {
                if(source.gameObject.name != "Music")
                {
                    soundSources.Add(source);
                    counter++;
                }
            }

            ClosePauseMenu();
        }

        // Update is called once per frame
        void Update()
        {
            if(Input.GetAxis("Pause") > 0)
            {
                OpenPauseMenu();    
            }
        }

        void OpenPauseMenu()
        {
            Input.ResetInputAxes();

            content.SetActive(true);
            confirmationPanel.SetActive(false);

            foreach(AudioSource source in soundSources)
            {
                source.Pause();
            }

            Time.timeScale = 0;
        }

        public void ClosePauseMenu()
        {
            content.SetActive(false);

            foreach (AudioSource source in soundSources)
            {
                source.UnPause();
            }

            Time.timeScale = 1;
        }
    }
}