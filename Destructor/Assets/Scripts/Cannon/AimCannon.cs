﻿    using UnityEngine;
using System.Collections;

namespace Netaphous.Destructor
{
    public class AimCannon : MonoBehaviour
    {
        // Private variables
        [SerializeField]
        private float turnSpeed = 0.5f;
        [SerializeField]
        private Vector2 xLimits;
        [SerializeField]
        private Vector2 yLimits;

        private Transform theTransform;

        // Use this for initialization
        void Start()
        {
            theTransform = transform;
        }

        // Update is called once per frame
        void Update()
        {
            float xAxis = -Input.GetAxis("Vertical");
            float yAxis = Input.GetAxis("Horizontal");

            Vector3 rotation = transform.rotation.eulerAngles;
            rotation.z = 0;

            Vector3 rot = transform.rotation.eulerAngles;

            if (rot.x > 180)
            {
                rot.x -= 180;
                rot.x = 180 - rot.x;
                rot.x = -rot.x;
            }

            if (rot.y > 180)
            {
                rot.y -= 180;
                rot.y = 180 -rot.y;
                rot.y = -rot.y;
            }

            if ((rot.x > xLimits.x && xAxis < 0) || (rot.x < xLimits.y && xAxis > 0))
            {
                rotation.x += xAxis * turnSpeed;

            }
            if ((rot.y > yLimits.x && yAxis < 0) || (rot.y < yLimits.y && yAxis > 0))
            {
                rotation.y += yAxis * turnSpeed;
            }

            theTransform.rotation = Quaternion.Euler(rotation);
        }
    }
}