﻿using UnityEngine;
using Netaphous.Utilities;
using System.Collections;
using System;

namespace Netaphous.Destructor
{
    public abstract class BasicShot : BasicPoolManager, IBasicShot
    {
        // Private variables
        // Script logic
        protected float shotPathAngle = 1.0f;
        private byte shotCost = 1;
        protected float forceModifier = 1.0f;
        private float shotDelay = 0.5f;
        private float lastShotTime = -100000.0f;
        private AudioClip shotSound;

        // Code optimisation
        protected GameObject newShot;
        ScoreHandler scoreHandIns;

        protected void Start()
        {
            ShotStats shotStats = GetComponent<ShotStats>();

            shotPathAngle = shotStats.shotPathAngle;
            shotCost = shotStats.pointsCost;
            forceModifier = shotStats.forceModifier;
            shotDelay = shotStats.shotDelay;
            shotSound = shotStats.firingSound;

            scoreHandIns = ScoreHandler.instance;

            CreatePool();
        }

        public abstract void Fire(float force, Transform spawnPoint);

        public void SetFired()
        {
            lastShotTime = Time.time;
        }

        public bool PayForShot()
        {
            if (scoreHandIns != null)
            {
                return scoreHandIns.SpendMoney(shotCost);
            }
            return false;
        }

        public float GetForceModifier()
        {
            return forceModifier;
        }

        public float GetShotPathAngle()
        {
            return shotPathAngle;
        }

        public int GetShotCost()
        {
            return shotCost;
        }

        public bool TestCanFire()
        {
            if (scoreHandIns.CanAfford(shotCost) &&
                Time.time > lastShotTime + shotDelay)
            {
                return true;
            }
            return false;
        }

        public string GetName()
        {
            return gameObject.name;
        }

        public AudioClip getShotClip()
        {
            return shotSound;
        }
    }
}