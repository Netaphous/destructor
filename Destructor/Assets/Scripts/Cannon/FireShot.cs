﻿using UnityEngine;
using System.Collections;

namespace Netaphous.Destructor
{
    public class FireShot : MonoBehaviour
    {
        // Public variables
        // To be accessed by other scripts
        [HideInInspector]
        public float force = 0.0f;

        // Private variables
        // Code optimisation
        private SwitchShot theSwitchShot;
        private Transform shotSpawn;
        private CannonHandler cannonHandler;
        private AudioSource audioSource;
        private AimPath aimPath;

        void Start()
        {
            theSwitchShot = GetComponentInChildren<SwitchShot>();

            aimPath = GetComponentInChildren<AimPath>();
            shotSpawn = aimPath.transform;

            cannonHandler = CannonHandler.instance;
            audioSource = GetComponent<AudioSource>();
        }

        void Update()
        {
            float fire = Input.GetAxis("ChargeAndFire");

            if (cannonHandler.chargeLocked &&
                fire > 0)
            {
                Fire();
            }
        }

        public void Fire()
        {
            cannonHandler.chargeLocked = false;
            cannonHandler.shotTypeLocked = false;
            theSwitchShot.currentShot.Fire(force, shotSpawn);
            audioSource.clip = theSwitchShot.currentShot.getShotClip();
            audioSource.Play();
            aimPath.HidePath();
        }
    }
}