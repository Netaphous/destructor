﻿using UnityEngine;
using System.Collections.Generic;

namespace Netaphous.Destructor
{
    public abstract class AreaEffect : MonoBehaviour
    {
        // Public variables
        // To be set in the editor
        public float size = 5.0f;
        public float growthSpeed = 0.1f;
        public float power = 10.0f;
        public float lifetime = 10.0f;

        // Private variables
        // Code optimisation
        protected Transform theTransform;
        private Vector3 startingScale;

        // Use this for initialization
        protected void Awake()
        {
            theTransform = transform;

            startingScale = theTransform.localScale;
        }

        protected void OnEnable()
        {
            theTransform.localScale = startingScale;

            Invoke("Kill", lifetime);
        }

        void OnDisable()
        {
            CancelInvoke("Kill");
        }

        void Update()
        {
            if (theTransform.localScale.x < size)
            {
                theTransform.localScale = new Vector3(theTransform.localScale.x + growthSpeed, theTransform.localScale.y + growthSpeed, theTransform.localScale.z + growthSpeed);
            }
        }

        void FixedUpdate()
        {
            StayAction();
        }

        void OnTriggerEnter(Collider other)
        {
            if (TestTag(other.tag))
            {
                EntryAction(other);
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (TestTag(other.tag))
            {
                ExitAction(other);
            }
        }

        protected bool TestTag(string tag)
        {
            if (tag == "Obstacle" || tag == "Shot" || tag == "Enemy" || tag == "Friendly")
            {
                return true;
            }
            return false;
        }
        protected void Kill()
        {
            KillAction();

            gameObject.SetActive(false);
        }

        protected abstract void EntryAction(Collider other);
        protected abstract void StayAction();
        protected abstract void ExitAction(Collider other);
        protected abstract void KillAction();
    }
}