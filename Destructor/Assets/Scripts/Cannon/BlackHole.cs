﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace Netaphous.Destructor
{
    public class BlackHole : AreaEffect
    {
        // Private variables
        // Accessible in the editor
        [SerializeField]
        private float explosionMultiplier = 2.5f;
        // Script logic
        private List<ConstantForce> objects;

        new void OnEnable()
        {
            base.OnEnable();
            objects = new List<ConstantForce>();
        }

        protected override void EntryAction(Collider other)
        {
            ConstantForce otherForce = other.gameObject.GetComponent<ConstantForce>();
            otherForce.enabled = true;
            EntityController otherHealth = other.gameObject.GetComponent<EntityController>();
            otherHealth.enabled = false;

            if (!objects.Contains(otherForce))
            {
                objects.Add(otherForce);
            }
        }

        protected override void StayAction()
        {
            foreach (ConstantForce obj in objects)
            {
                if (obj != null)
                {
                    Vector3 direction = theTransform.position - obj.transform.position;
                    direction.Normalize();

                    obj.force = direction * power;
                }
            }
        }

        protected override void ExitAction(Collider other)
        {
            ConstantForce otherForce = other.gameObject.GetComponent<ConstantForce>();
            otherForce.enabled = false;

            EntityController otherHealth = other.gameObject.GetComponent<EntityController>();
            otherHealth.enabled = true;

            if (objects.Contains(otherForce))
            {
                objects.Remove(otherForce);
            }
        }

        protected override void KillAction()
        {
            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i] != null)
                {
                    objects[i].enabled = false;

                    Vector3 direction = theTransform.position - objects[i].transform.position;

                    objects[i].GetComponent<Rigidbody>().AddForce(-direction * power * explosionMultiplier);

                    if (objects[i].tag != "Enemy" && objects[i].tag != "Friendly")
                    {
                        objects[i].GetComponent<Collider>().enabled = false;

                        EntityController otherHealth = objects[i].GetComponent<EntityController>();
                        otherHealth.enabled = true;

                        objects[i].gameObject.SendMessage("ColliderOnDelay");
                    }
                }
            }
        }
    }
}