﻿using UnityEngine;
using System.Collections;

namespace Netaphous.Destructor
{
    public class AdjustForce : MonoBehaviour
    {
        // Public variables
        // To be set in the editor
        [HideInInspector]
        public AimPath theAimPath;

        // Private variables
        // Accessible in the editor
        [SerializeField]
        private float minPower = 5.0f;
        [SerializeField]
        private float maxPower = 20.0f;
        // Script logic
        private float curveValue = 0.0f;
        private float force = 0.0f;
        private float forceModifier = 0.0f;

        // Code optimisation
        private FireShot theFireShot;

        void Awake()
        {
            theFireShot = GetComponent<FireShot>();
            theAimPath = GetComponentInChildren<AimPath>();
        }

        public void SetForce(float curveValue)
        {
            this.curveValue = curveValue;

            UpdateForce();
        }

        public void SetForceModifier(float forceModifier)
        {
            this.forceModifier = forceModifier;

            UpdateForce();
        }

        public void UpdateForce()
        {
            CalculateForce();

            theAimPath.force = force;
            theFireShot.force = force;

            float forceAsPercentage = (force - (minPower * forceModifier)) / ((maxPower - minPower) * forceModifier);

            GUIHandler.instance.UpdateForce(forceAsPercentage);
        }

        public void CalculateForce()
        {
            float powerDifference = maxPower - minPower;

            force = (powerDifference * curveValue + minPower) * forceModifier;
        }
    }
}