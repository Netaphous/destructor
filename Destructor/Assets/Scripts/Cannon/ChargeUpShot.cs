﻿using UnityEngine;
using System.Collections;

namespace Netaphous.Destructor
{
    public class ChargeUpShot : MonoBehaviour
    {
        // Private variables
        [SerializeField]
        private AnimationCurve chargeSpeedCurve;
        [SerializeField]
        private float chargeSpeed = 0.1f;
        [SerializeField]
        private AudioClip chargeLockClip;

        // Script logic
        private bool isCharging = false;
        private bool isClimbing = false;
        private float currentPointOnCurve = 0.0f;

        // Code optimisation
        private AdjustForce forceHandler;
        private SwitchShot theSwitchShot;
        private float curveValue = 0.0f;
        private CannonHandler cannonHandler;
        private AudioSource audioSource;
        private AimPath aimPath;

        void Start()
        {
            forceHandler = GetComponent<AdjustForce>();
            theSwitchShot = GetComponentInChildren<SwitchShot>();
            cannonHandler = CannonHandler.instance;
            audioSource = GetComponent<AudioSource>();
            aimPath = GetComponentInChildren<AimPath>();
        }

        void FixedUpdate()
        {
            if (theSwitchShot.currentShot.TestCanFire() &&
                !cannonHandler.chargeLocked)
            {
                float fire = Input.GetAxis("ChargeAndFire");

                if (!isCharging && fire > 0)
                {
                    StartCharge();
                }
                else if (isCharging)
                {
                    Charging();
                    if (fire == 0)
                    {
                        LockCharge();
                    }
                }

                UpdateForce();
            }
        }

        private void StartCharge()
        {
            aimPath.hidden = false;
            isCharging = true;
            isClimbing = true;
            currentPointOnCurve = 0.0f;
            cannonHandler.shotTypeLocked = true;
        }

        private void Charging()
        {
            if (isClimbing)
            {
                currentPointOnCurve += chargeSpeed;
            }
            else
            {
                currentPointOnCurve -= chargeSpeed;
            }

            if (currentPointOnCurve >= 1 || currentPointOnCurve <= 0)
            {
                isClimbing = !isClimbing;
            }
        }

        private void UpdateForce()
        {
            curveValue = chargeSpeedCurve.Evaluate(currentPointOnCurve);

            forceHandler.SetForce(curveValue);
        }

        private void LockCharge()
        {
            isCharging = false;

            cannonHandler.chargeLocked = true;

            audioSource.clip = chargeLockClip;
            audioSource.Play();
        }
    }
}