﻿using UnityEngine;
using System.Collections;

namespace Netaphous.Destructor
{
    public class ShotStats : MonoBehaviour
    {
        public float shotPathAngle = 1.0f;
        public byte pointsCost = 1;
        public float forceModifier = 1.0f;
        public float shotDelay = 0.5f;
        public AudioClip firingSound;
    }
}