﻿using UnityEngine;
using System.Collections;

namespace Netaphous.Destructor
{
    public class SwitchShot : MonoBehaviour
    {
        // Public variables
        // To be set in the editor
        public IBasicShot currentShot;

        // Private variables
        // Script logic
        private int currentShotIndex = 0;
        private IBasicShot[] shotTypes;
        private float prevInput = 0.0f;

        // Code optimisation
        private AdjustForce forceHandler;
        private CannonHandler cannonHandler;
        private GUIHandler UIHandler;

        // Use this for initialization
        void Start()
        {
            forceHandler = transform.parent.GetComponent<AdjustForce>();

            shotTypes = GetComponentsInChildren<IBasicShot>();

            cannonHandler = CannonHandler.instance;

            UIHandler = GUIHandler.instance;

            UpdateShotType();

            currentShotIndex++;

            Invoke("EquipPreviousShot", 0.1f);
        }

        // Update is called once per frame
        void Update()
        {
            float shotChange = Input.GetAxis("ChangeShot");

            if (!cannonHandler.shotTypeLocked && 
                shotChange != prevInput)
            {
                if (shotChange > 0)
                {
                    EquipNextShot();
                }
                else if (shotChange < 0)
                {
                    EquipPreviousShot();
                }
            }
            prevInput = shotChange;
        }

        private void EquipPreviousShot()
        {
            currentShotIndex--;

            if (currentShotIndex < 0)
            {
                currentShotIndex = shotTypes.Length - 1;
            }

            UpdateShotType();
        }

        private void EquipNextShot()
        {
            currentShotIndex++;

            if (currentShotIndex >= shotTypes.Length)
            {
                currentShotIndex = 0;
            }

            UpdateShotType();
        }

        private void UpdateShotType()
        {
            currentShot = shotTypes[currentShotIndex];

            UIHandler.UpdateShot(currentShot);

            forceHandler.SetForceModifier(currentShot.GetForceModifier());

            forceHandler.theAimPath.angleAdjustment = currentShot.GetShotPathAngle();
        }
    }
}