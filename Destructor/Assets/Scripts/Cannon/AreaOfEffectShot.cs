﻿using UnityEngine;
using System.Collections;
using System;

namespace Netaphous.Destructor
{
    public class AreaOfEffectShot : BasicShot, IAreaShot
    {
        // Private variables
        // Accessible in the editor
        [SerializeField]
        private GameObject wellPrefab;
        [SerializeField]
        private float wellSize = 10.0f;
        [SerializeField]
        private float wellGrowthSpeed = 0.1f;
        [SerializeField]
        private float wellPower = 10.0f;
        [SerializeField]
        private float wellLifetime = 10.0f;

        // Script logic
        private GameObject[] areaEffectWell;

        // Code optimisation
        private GameObject newWell;

        new void Start()
        {
            base.Start();
            CreateWellPool();
        }

        private void CreateWellPool()
        {
            areaEffectWell = new GameObject[pooledAmount];

            for (int i = 0; i < pooledAmount; i++)
            {
                areaEffectWell[i] = Instantiate(wellPrefab) as GameObject;

                AreaEffect effect = areaEffectWell[i].GetComponent<AreaEffect>();

                effect.size = wellSize;
                effect.growthSpeed = wellGrowthSpeed;
                effect.power = wellPower;
                effect.lifetime = wellLifetime;
                
                areaEffectWell[i].SetActive(false);
            }
        }

        public GameObject GetPooledWellItem()
        {
            for (int i = 0; i < pooledAmount; i++)
            {
                if (areaEffectWell[i] != null &&
                    !areaEffectWell[i].activeInHierarchy)
                {
                    return areaEffectWell[i];
                }
            }
            return null;
        }

        private void ResetWellPool()
        {
            for (int i = 0; i < pooledAmount; i++)
            {
                if (areaEffectWell[i] != null &&
                    areaEffectWell[i].activeInHierarchy)
                {
                    areaEffectWell[i].SetActive(false);
                }
            }
        }

        public override void Fire(float force, Transform spawnPoint)
        {
            if (TestCanFire())
            {
                FireAreaShot(force, spawnPoint);
            }
        }

        public void FireAreaShot(float force, Transform spawnPoint)
        {
            newShot = GetPooledItem();

            if (newShot != null &&
                    PayForShot())
            {
                newShot.SetActive(true);
                newShot.transform.position = spawnPoint.position;
                newShot.transform.rotation = spawnPoint.rotation;

                newShot.GetComponent<AOEProjectile>().shotParent = this;

                Rigidbody otherRigid = newShot.GetComponent<Rigidbody>();

                otherRigid.velocity = Vector3.zero;

                otherRigid.AddForce(newShot.transform.up * force, ForceMode.Impulse);

                newShot = null;

                SetFired();
            }
        }
    }
}