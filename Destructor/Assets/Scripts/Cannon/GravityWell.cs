﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace Netaphous.Destructor
{
    public class GravityWell : AreaEffect
    {
        // Private variables
        // Script logic
        private List<ConstantForce> objects;

        new void OnEnable()
        {
            base.OnEnable();
            objects = new List<ConstantForce>();
        }

        protected override void EntryAction(Collider other)
        {
            ConstantForce otherForce = other.gameObject.GetComponent<ConstantForce>();
            otherForce.enabled = true;

            otherForce.force = -Physics.gravity * power;

            if (!objects.Contains(otherForce))
            {
                objects.Add(otherForce);
            }
        }

        protected override void StayAction()
        {
            // Unnecessary so do nothing
        }

        protected override void ExitAction(Collider other)
        {
            ConstantForce otherForce = other.gameObject.GetComponent<ConstantForce>();
            otherForce.enabled = false;
            otherForce.force = Vector3.zero;

            if (objects.Contains(otherForce))
            {
                objects.Remove(otherForce);
            }
        }

        protected override void KillAction()
        {
            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i] != null)
                {
                    objects[i].enabled = false;
                }
            }
        }
    }
}