﻿using UnityEngine;
using System.Collections;

namespace Netaphous.Destructor
{
    public class AimPath : MonoBehaviour
    {
        // Public variables
        // To be set in the editor
        public float angleAdjustment = 1.0f;
        public bool hidden = true;

        // To be accessed by other scripts
        [HideInInspector]
        public float force = 5.0f;

        // Private variables
        // Accessible in the editor
        [SerializeField]
        private GameObject dot;
        [SerializeField]
        private int numOfDots = 10;
        [SerializeField]
        private float dotSpacing = 0.1f;

        // Script logic                
        private GameObject[] line;

        // Use this for initialization
        void Awake()
        {
            CreateLine();
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (!hidden)
            {
                Aim(transform.up);
            }
        }

        private void CreateLine()
        {
            line = new GameObject[numOfDots];

            for (int i = 0; i < numOfDots; i++)
            {
                GameObject tempObject = Instantiate(dot, transform, true) as GameObject;

                line[i] = tempObject;
            }
        }

        private void Aim(Vector3 direction)
        {
            float VoX = direction.x * force * angleAdjustment;
            float VoY = direction.y * force * angleAdjustment;
            float VoZ = direction.z * force * angleAdjustment;

            for (int i = 0; i < numOfDots; i++)
            {
                float t = i * dotSpacing;

                float vX = VoX * t;
                float vZ = VoZ * t;
                float vY = (float) ((VoY * t) - (0.5 * -Physics.gravity.y * (t * t)));

                line[i].transform.position = new Vector3(transform.position.x + vX, transform.position.y + vY, transform.position.z + vZ);

                line[i].SetActive(true);
            }
        }

        public void HidePath()
        {
            hidden = true;
            for (int i = 0; i < line.Length; i++)
            {
                line[i].SetActive(false);
            }
        }
    }
}