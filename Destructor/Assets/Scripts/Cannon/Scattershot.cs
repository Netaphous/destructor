﻿using UnityEngine;
using System.Collections;

namespace Netaphous.Destructor
{
    public class Scattershot : BasicShot, IMultipleShots
    {
        // Private variables
        // Accessible in the editor
        [SerializeField]
        private float projectileCount = 5;
        [SerializeField]
        private float rotationVariation = 10.0f;

        public override void Fire(float force, Transform spawnPoint)
        {
            if (TestCanFire())
            {
                FireMultiple(force, spawnPoint);
            }
        }

        public void FireMultiple(float force, Transform spawnPoint)
        {
            for(int i = 0; i < projectileCount; i++)
            {
                FireSingleShot(force, spawnPoint);
            }
        }

        public void FireSingleShot(float force, Transform spawnPoint)
        {
            float xRot = Random.Range(-rotationVariation, rotationVariation);
            float zRot = Random.Range(-rotationVariation, rotationVariation);

            Quaternion newRot = Quaternion.Euler(new Vector3(
                spawnPoint.rotation.eulerAngles.x + xRot,
                spawnPoint.rotation.eulerAngles.y,
                spawnPoint.rotation.eulerAngles.z + zRot));

            newShot = GetPooledItem();

            if (newShot != null && 
                PayForShot())
            {
                newShot.SetActive(true);
                newShot.transform.position = spawnPoint.position;
                newShot.transform.rotation = newRot;

                newShot.GetComponent<Collider>().enabled = false;

                newShot.GetComponent<Rigidbody>().AddForce(newShot.transform.up * force, ForceMode.Impulse);

                newShot = null;
                
                SetFired();
            }
        }
    }
}