﻿using UnityEngine;
using System.Collections;

namespace Netaphous.Destructor
{
    public class AOEProjectile : MonoBehaviour
    {
        // Public variables
        // To be accessed by other scripts
        [HideInInspector]
        public AreaOfEffectShot shotParent;

        void OnCollisionEnter(Collision other)
        {
            Rigidbody otherRigid = other.gameObject.GetComponent<Rigidbody>();

            if (otherRigid != null)
            {
                otherRigid.velocity = Vector3.zero;
            }

            GameObject well = shotParent.GetPooledWellItem();

            well.transform.position = transform.position;

            well.SetActive(true);

            gameObject.SetActive(false);
        }
    }
}