﻿using UnityEngine;
using System.Collections;
using System;
using Netaphous.Utilities;

namespace Netaphous.Destructor
{
    public class NormalShot : BasicShot
    {        
        public override void Fire(float force, Transform spawnPoint)
        {
            if (TestCanFire())
            {
                newShot = GetPooledItem();

                if (newShot != null && 
                    PayForShot())
                {
                    newShot.transform.position = spawnPoint.position;
                    newShot.transform.rotation = spawnPoint.rotation;

                    newShot.SetActive(true);

                    Rigidbody otherRigid = newShot.GetComponent<Rigidbody>();

                    otherRigid.velocity = Vector3.zero;

                    otherRigid.AddForce(newShot.transform.up * force, ForceMode.Impulse);

                    newShot = null;

                    SetFired();
                }
            }
        }
    }
}