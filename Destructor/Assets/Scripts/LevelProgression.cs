﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Netaphous.Destructor
{
    public class LevelProgression : MonoBehaviour
    {
        // Public variables
        // Static variables
        public static int currentLevel = 1;

        // Private variables
        // Accessible in the editor
        [SerializeField]
        private Button[] levelButtons;

        void Start()
        {
            ActivateButtons();
        }

        public void ActivateButtons()
        {
            for (int i = 1; i <= currentLevel; i++)
            {
                if(i > levelButtons.Length)
                {
                    break;
                }
                print("Level " + i + " enabled");
                levelButtons[i - 1].interactable = true;
            }
        }
    }
}