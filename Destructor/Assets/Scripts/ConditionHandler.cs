﻿using UnityEngine;
using Netaphous.Utilities;
using System.Collections.Generic;
using System.Collections;

namespace Netaphous.Destructor
{
    public class ConditionHandler : MonoBehaviour
    {
        // Public variables
        // Static variables
        public static ConditionHandler instance;

        // Private variables
        private List<GameObject> enemies;

        void Awake()
        {
            if(instance != null)
            {
                Destroy(this);
            }
            else
            {
                instance = this;
            }
            DontDestroyOnLoad(instance);
        }

        void OnEnable()
        {
            enemies = new List<GameObject>();

            EventManager.StartListening("StartLevel", StartLevel);
        }

        public void StartLevel()
        {
            enemies = new List<GameObject>(GameObject.FindGameObjectsWithTag("Enemy"));

            GUIHandler.instance.UpdateEnemies(enemies.Count);

            StartCoroutine(TestForVictory());
        }
        
        IEnumerator TestForVictory()
        {
            while (enemies.Count > 0)
            {
                yield return null;
            }
            Victory();
        }

        public void Remove(GameObject target)
        {
            switch (target.tag)
            {
                case "Friendly":
                    GameOver();
                    break;
                case "Enemy":
                    enemies.Remove(target);
                    GUIHandler.instance.UpdateEnemies(enemies.Count);
                    break;
            }
        }

        public void EndGame()
        {
            GameOver();
        }

        void GameOver()
        {
            EventManager.TriggerEvent("GameOver");
            Time.timeScale = 0;
        }

        void Victory()
        {
            EventManager.TriggerEvent("Victory");
            Time.timeScale = 0;
        }
    }
}