﻿using UnityEngine;
using System.Collections;
using Netaphous.Utilities;

namespace Netaphous.Destructor
{
    public class CameraController : MonoBehaviour
    {
        // Public variables
        // Static variables
        public static CameraController instance;

        // Private variables
        // Accessible in the editor
        private Camera[] cameras;

        // Script logic
        private float previousInput = 0.0f;
        private int currentCamera = 0;

        void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
            }
            else
            {
                instance = this;
            }
            DontDestroyOnLoad(instance);
        }

        void OnEnable()
        {
            EventManager.StartListening("StartLevel", StartLevel);
        }

        public void StartLevel()
        {
            GameObject[] tempArray = GameObject.FindGameObjectsWithTag("MainCamera");

            cameras = new Camera[tempArray.Length];

            for (int i = 0; i < tempArray.Length; i++)
            {
                cameras[i] = tempArray[i].GetComponent<Camera>();
                if (cameras[i].name == "Cannon Camera")
                {
                    currentCamera = i - 1;
                }
            }
            SwitchCamera();            
        }

        // Update is called once per frame
        void Update()
        {
            float input = Input.GetAxis("SwitchCamera");

            if (input != previousInput &&
                input == 1)
            {
                SwitchCamera();
            }

            previousInput = input;
        }

        private void SwitchCamera()
        {
            currentCamera++;
            if (currentCamera >= cameras.Length)
            {
                currentCamera = 0;
            }
            cameras[currentCamera].enabled = true;

            for (int i = 0; i < cameras.Length; i++)
            {
                if (i != currentCamera)
                {
                    cameras[i].enabled = false;
                }
            }
        }
    }

}