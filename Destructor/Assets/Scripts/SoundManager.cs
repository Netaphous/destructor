﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    [SerializeField]
    private AudioMixer masterMixer;

    private SoundManager instance;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(instance);
    }

    public void SetSoundVolume(float volume)
    {
        float newVol = 0;
        if (volume == 0)
        {
            newVol = -80.0f;
        }
        else
        {
            newVol = volume * 40;
            newVol = -40.0f + newVol;
        }
        masterMixer.SetFloat("SoundVol", newVol);
    }

    public void SetMusicVolume(float volume)
    {
        float newVol = 0;
        if (volume == 0)
        {
            newVol = -80.0f;
        }
        else
        {
            newVol = volume * 40;
            newVol = -40.0f + newVol;
        }
        masterMixer.SetFloat("MusicVol", newVol);
    }
}
