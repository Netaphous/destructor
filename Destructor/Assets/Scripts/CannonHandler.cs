﻿using UnityEngine;
using System.Collections;

namespace Netaphous.Destructor
{
    public class CannonHandler : MonoBehaviour
    {
        // Public variables
        // Static variables
        public static CannonHandler instance;

        // To be used by other scripts
        [HideInInspector]
        public bool chargeLocked = false;
        [HideInInspector]
        public bool shotTypeLocked = false;

        void Awake()
        {
            instance = this;
        }
    }
}