﻿using UnityEngine;
using System.Collections;

namespace Netaphous.Destructor
{
    public class ResetOnDisable : MonoBehaviour
    {
        void OnDisable()
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }
}