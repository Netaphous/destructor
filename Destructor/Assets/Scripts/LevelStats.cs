﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Netaphous.Utilities;

namespace Netaphous.Destructor
{
    public class LevelStats : MonoBehaviour
    {
        // Private variables
        // Accessible in the editor
        [SerializeField]
        private int startingMoney = 10;
        [SerializeField]
        private int levelNumber = 1;

        // Use this for initialization
        void OnEnable()
        {
            ScoreHandler.instance.SetMoney(startingMoney);

            EventManager.StartListening("Victory", RegisterComplete);

            Time.timeScale = 1;
        }

        void Start()
        {
            EventManager.TriggerEvent("StartLevel");
        }

        void OnDisable()
        {
            EventManager.StopListening("Victory", RegisterComplete);
        }

        void RegisterComplete()
        {
            if(LevelProgression.currentLevel <= levelNumber)
            {
                LevelProgression.currentLevel = levelNumber + 1;
            }
        }
    }
}