﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using Netaphous.Utilities;

namespace Netaphous.Destructor
{
    public class GUIHandler : MonoBehaviour
    {
        // Public variables
        // Static variables
        public static GUIHandler instance;

        // Private variables
        // Accessible in the editor        
        // HUD elements
        [SerializeField]
        private GameObject HUDPanel;
        [SerializeField]
        private GameObject[] shotTypeImages;
        [SerializeField]
        private Text shotTypeText;
        [SerializeField]
        private Text shotTypeCost;
        [SerializeField]
        private Text totalMoney;
        [SerializeField]
        private Text enemyText;
        [SerializeField]
        private GameObject cantAffordImage;
        [SerializeField]
        private Slider forceSlider;

        // Window elements
        [SerializeField]
        private GameObject victoryPanel;
        [SerializeField]
        private Text victoryText;
        [SerializeField]
        private GameObject gameOverPanel;
        [SerializeField]
        private Text gameOverText;
        [SerializeField]
        private PauseHandler pauseMenu;

        private int moneyLeft = 0;
        private int enemiesLeft = 0;

        // Unity Methods
        void Awake()
        {
            if (instance != null)
            {
                Destroy(this);
            }
            else
            {
                instance = this;
            }
            DontDestroyOnLoad(instance);
        }

        void OnEnable()
        {
            EventManager.StartListening("GameOver", GameOver);
            EventManager.StartListening("Victory", Victory);
            EventManager.StartListening("StartLevel", StartLevel);
        }

        void OnDisable()
        {
            EventManager.StopListening("GameOver", GameOver);
            EventManager.StopListening("Victory", Victory);
            EventManager.StopListening("StartLevel", StartLevel);
        }

        // Public methods
        public void HideGUI()
        {
            HUDPanel.SetActive(false);
            victoryPanel.SetActive(false);
            gameOverPanel.SetActive(false);
            pauseMenu.ClosePauseMenu();
        }

        public void UpdateForce(float chargePercentage)
        {
            forceSlider.value = 1 - chargePercentage;
        }

        public void UpdateShot(IBasicShot shotType)
        {
            for (int i = 0; i < shotTypeImages.Length; i++)
            {
                if (shotTypeImages[i].name == shotType.GetName())
                {
                    shotTypeImages[i].SetActive(true);
                    shotTypeText.text = shotType.GetName().ToUpper();
                    if(shotType.GetName() == "Scatter Shot")
                    {
                        shotTypeCost.text = "SHOT COST: £ " + shotType.GetShotCost() * 5;
                    }
                    shotTypeCost.text = "SHOT COST: £ " + shotType.GetShotCost();
                    UpdateShotCost(shotType.GetShotCost());
                }
                else
                {
                    shotTypeImages[i].SetActive(false);
                }
            }
        }

        public void UpdateMoney(int money)
        {
            totalMoney.text = "MONEY LEFT: £ " + money;
            moneyLeft = money;
        }

        public void UpdateEnemies(int count)
        {
            enemyText.text = "ENEMIES LEFT: " + count;
            enemiesLeft = count;
        }

        // Private methods
        private void UpdateShotCost(int cost)
        {
            if (ScoreHandler.instance.CanAfford(cost))
            {
                cantAffordImage.SetActive(false);
            }
            else
            {
                cantAffordImage.SetActive(true);
            }
        }
        
        private void StartLevel()
        {
            HUDPanel.SetActive(true);
        }

        private void Victory()
        {
            victoryPanel.SetActive(true);

            victoryText.text = "MONEY LEFT: £ " + moneyLeft;
        }

        private void GameOver()
        {
            gameOverPanel.SetActive(true);

            gameOverText.text = "ENEMIES LEFT: " + enemiesLeft;
        }
    }
}