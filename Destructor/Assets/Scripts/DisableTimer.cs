﻿using UnityEngine;
using System.Collections;

namespace Netaphous.Destructor
{
    public class DisableTimer : MonoBehaviour
    {
        [SerializeField]
        private float disableTime = 10.0f;

        // Use this for initialization
        void OnEnable()
        {
            Invoke("DisableObject", disableTime);
        }

        void OnDisable()
        {
            CancelInvoke();
        }

        void DisableObject()
        {
            gameObject.SetActive(false);
        }
    }
}
