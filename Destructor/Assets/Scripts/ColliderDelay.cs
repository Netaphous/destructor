﻿using UnityEngine;
using System.Collections;

namespace Netaphous.Destructor
{
    public class ColliderDelay : MonoBehaviour
    {
        [SerializeField]
        private float delay = 1.0f;
        [SerializeField]
        private bool onStart = false;

        private Collider theCollider;

        void Awake()
        {
            theCollider = GetComponent<Collider>();
        }

        // Use this for initialization
        void OnEnable()
        {
            if(onStart)
            {
                ColliderOnDelay();
            }
        }

        void OnDisable()
        {
            CancelInvoke();
            theCollider.enabled = false;
        }

        public void ColliderOnDelay()
        {
            Invoke("ColliderOn", delay);
        }

        private void ColliderOn()
        {
            theCollider.enabled = true;
        }
    }
}