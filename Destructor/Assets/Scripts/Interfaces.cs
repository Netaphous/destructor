﻿using UnityEngine;
using System.Collections;

namespace Netaphous.Destructor
{
    public interface IMultipleShots
    {
        void FireMultiple(float force, Transform spawnPoint);
        void FireSingleShot(float force, Transform spawnPoint);
    }

    public interface IBasicShot
    {
        void Fire(float force, Transform spawnPoint);
        bool PayForShot();
        void SetFired();
        float GetForceModifier();
        float GetShotPathAngle();
        int GetShotCost();
        bool TestCanFire();
        string GetName();
        AudioClip getShotClip();
    }

    public interface IAreaShot
    {
        void FireAreaShot(float force, Transform spawnPoint);
    }
}