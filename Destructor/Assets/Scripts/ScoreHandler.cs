﻿using UnityEngine;
using System.Collections;
using Netaphous.Utilities;

namespace Netaphous.Destructor
{
    public class ScoreHandler : MonoBehaviour
    {
        // Public variables
        // Static variables
        public static ScoreHandler instance
        {
            get
            {
                if (scoreHandler == null)
                {
                    Debug.LogError("There needs to be one active ScoreHandler script on a GameObject in your scene.");
                }

                return scoreHandler;
            }
        }

        // Private variables
        // Static variables
        private static ScoreHandler scoreHandler;

        // Script logic
        private int money = 0;

        void Awake()
        {
            if (scoreHandler != null)
            {
                Destroy(this);
            }
            else
            {
                scoreHandler = this;
            }
            DontDestroyOnLoad(scoreHandler);
        }

        void OnEnable()
        {
            EventManager.StartListening("Victory", Victory);
        }

        void OnDisable()
        {
            EventManager.StopListening("Victory", Victory);
        }

        private void Victory()
        {
            CancelInvoke();
        }

        public bool SpendMoney(int cost)
        {
            bool blnResult = false;

            if(CanAfford(cost))
            {
                money -= cost;
                blnResult = true;
            }
            if(money <= 0)
            {
                Invoke("OutOfPoints", 10.0f);
            }

            GUIHandler.instance.UpdateMoney(money);

            return blnResult;
        }

        private void OutOfPoints()
        {
            ConditionHandler.instance.EndGame();
        }

        public void SetMoney(int newMoney)
        {
            money = newMoney;
            GUIHandler.instance.UpdateMoney(money);
        }

        public bool CanAfford(int cost)
        {
            return money >= cost;
        }
    }
}