﻿using UnityEngine;
using System.Collections;
using System;

namespace Netaphous.Destructor
{
    public class EntityController : MonoBehaviour
    {
        // Private variables
        [SerializeField]
        private float health = 10.0f;
        [SerializeField]
        private string particleName;
        [SerializeField]
        private int particleCount = 20;

        // Script logic
        private ParticleSystem theParticleSystem;

        // Code optimisation
        private Transform theTransform;
        private Rigidbody theRigidbody;
        private AudioSource theAudioSource;

        void Start()
        {
            theTransform = transform;
            theRigidbody = GetComponent<Rigidbody>();
            theAudioSource = GetComponent<AudioSource>();

            ParticleSystem[] particleSystems = FindObjectsOfType<ParticleSystem>();

            foreach (ParticleSystem obj in particleSystems)
            {
                if(obj.name == particleName)
                {
                    theParticleSystem = obj;
                    break;
                }
            }
        }

        void Update()
        {
            if(theTransform.position.y < -50)
            {
                Kill();
            }
        }

        void OnCollisionEnter(Collision other)
        {
            if (enabled == true && 
                (other.gameObject.tag == "Obstacle" || other.gameObject.tag == "Shot"))
            {
                Rigidbody rigid = other.gameObject.GetComponent<Rigidbody>();
                health -= (rigid.mass * rigid.velocity.magnitude) + (theRigidbody.mass * theRigidbody.velocity.magnitude);

                if (health <= 0)
                {
                    Kill();
                }
            }
        }

        void Kill()
        {
            if(theParticleSystem != null)
            {
                theParticleSystem.transform.position = theTransform.position;
                theParticleSystem.transform.rotation = theTransform.rotation;
                theTransform.transform.localScale = theTransform.localScale;
                theParticleSystem.Emit(particleCount);
            }

            ConditionHandler.instance.Remove(gameObject);

            if (theAudioSource != null)
            {
                theAudioSource.Play();
                GetComponent<Renderer>().enabled = false;
                GetComponent<Collider>().enabled = false;
                Invoke("RealKill", theAudioSource.clip.length);
            }
            else
            {
                RealKill();
            }
        }

        private void RealKill()
        {
            Destroy(gameObject);
        }
    }
}