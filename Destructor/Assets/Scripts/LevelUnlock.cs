﻿using UnityEngine;
using System.Collections;

namespace Netaphous.Destructor
{
    public class LevelUnlock : MonoBehaviour
    {
        private int counter = 0;

        public void AddCounter() 
        {
            counter++;
            if(counter >= 4)
            {
                ActivateButtons();
            }
        }

        private void ActivateButtons()
        {
            LevelProgression.currentLevel = 999;
        }
    }
}