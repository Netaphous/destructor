﻿using UnityEngine;
using System.Collections;


namespace Netaphous.Utilities
{
    public class DontDestroyOnLoad : MonoBehaviour
    {
        void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}